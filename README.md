# Snowman

☃️  ⟶ 🥔 ☃️

### We can make him tall. Or we can make him not so tall...

[Snowman](https://youtu.be/JQ1ZOFNBL68?t=8) is a seed project for [Shpadoinkle](https://gitlab.com/fresheyeball/Shpadoinkle/-/tree/master/#shpadoinkle).

[![Status](https://gitlab.com/fresheyeball/Shpadoinkle-snowman/badges/master/pipeline.svg)](https://gitlab.com/fresheyeball/Shpadoinkle-snowman/-/pipelines)

## Get the project

To create a new Snowman project:

```bash
bash <( curl https://gitlab.com/fresheyeball/Shpadoinkle-snowman/-/raw/master/generator.sh )
```

This project supports building with both `nix` and `stack`.

## Stack

Stack should work out of the box, provided you have the following installed on your system:

- git
- zlib
- zlib-dev
- pcre

```bash
stack build
```

## Nix

This project is built with [Nix](https://nixos.org/). [Visit the Shpadoinkle guide for Nix.](https://fresheyeball.gitlab.io/Shpadoinkle/docs/getting-started/index.html#_nix)

## Build the project

The included `default.nix` file has some arguments to customize your build. To build with GHC

```bash
nix-build
```

To build with GHCjs

```bash
nix-build --arg isJS true
```

> Note: If you run into build errors, try updating the `chan` hash in `default.nix` to the latest one identified [here](https://gitlab.com/fresheyeball/Shpadoinkle/-/blob/master/nix/chan.nix)

## Develop

```
nix-shell
```

Will drop you into a dev shell with [Ghcid](https://github.com/ndmitchell/ghcid#ghcid----) and other common haskell development tools.

### TLDR

Get a ghcid server with live reloads in one line

```bash
nix-shell --command "ghcid --command 'cabal repl' -W -T Main.dev"
```

Get a hoogle server in one line

```bash
nix-shell --arg withHoogle true --command "hoogle serve"
```

### License

[![CC0](https://licensebuttons.net/p/zero/1.0/80x15.png)](http://creativecommons.org/publicdomain/zero/1.0/)

Isaac Shapira has waived all copyright and related or neighboring rights to Shpadoinkle Snowman.
This work is published from: United States
